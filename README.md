LedGontrol - RPi LedControl library written in Go.

This library is a port (with different API, but same functionality) of the popular Arduino LedControl library.
It's written in pure Go in order to allow for easier cross-compilation.

**NOT YET IMPLEMENTED**:

setColumn
setLed
setDigit
setChar