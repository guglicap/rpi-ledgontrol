package main

import (
	ledgontrol "gitlab.com/guglicap/rpi-ledgontrol"
	"log"
	"os"
)

func main() {
	mtx, err := ledgontrol.NewMAX7219(10, 11, 8)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	mtx.Shutdown(false)
	for i := 0; i < 4; i++ {
		mtx.SetRow(i, 0xff)
	}
}
