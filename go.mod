module gitlab.com/guglicap/rpi-ledgontrol

go 1.14

require github.com/stianeikeland/go-rpio/v4 v4.4.0
