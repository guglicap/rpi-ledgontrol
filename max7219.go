package ledgontrol

import (
	"fmt"
	"github.com/stianeikeland/go-rpio/v4"
)

type OPCode uint8

const (
	OPNoop        OPCode = 0
	OPDigit0             = 1
	OPDigit1             = 2
	OPDigit2             = 3
	OPDigit3             = 4
	OPDigit4             = 5
	OPDigit5             = 6
	OPDigit6             = 7
	OPDigit7             = 8
	OPDecodeMode         = 9
	OPIntensity          = 10
	OPScanLimit          = 11
	OPShutdown           = 12
	OPDisplayTest        = 15
)

var rows = []OPCode{OPDigit0, OPDigit1, OPDigit2, OPDigit3, OPDigit4, OPDigit5, OPDigit6, OPDigit7}

type MAX7219 struct {
	dPin, clkPin, csPin rpio.Pin
	addr                int
}

func NewMAX7219(dPin, clkPin, csPin int) (max7219 *MAX7219, err error) {
	err = rpio.Open()
	if err != nil {
		err = fmt.Errorf("error initializing gpio: %w", err)
		return
	}
	max7219 = &MAX7219{
		dPin:   rpio.Pin(dPin),
		clkPin: rpio.Pin(clkPin),
		csPin:  rpio.Pin(csPin),
		addr:   0,
	}
	max7219.init()
	return
}

func (m *MAX7219) init() {
	m.dPin.Output()
	m.clkPin.Output()
	m.csPin.Output()
	m.csPin.High()
	m.DisplayTest(false)
	m.DecodeMode(false)
	m.SetScanLimit(7)
	m.ClearDisplay()
	m.Shutdown(true)
}

func (m *MAX7219) sendCommand(opCode OPCode, v uint8) {
	m.csPin.Low()
	ShiftOut(m.dPin, m.clkPin, MSBFirst, uint8(opCode))
	ShiftOut(m.dPin, m.clkPin, MSBFirst, v)
	for m.addr > 0 {
		m.addr -= 1
		m.sendCommand(OPNoop, 0)
	}
	m.csPin.High()
}

func (m *MAX7219) Select(addr int) *MAX7219 {
	m.addr = addr
	return m
}

func (m *MAX7219) SetIntensity(intensity uint8) {
	if intensity > 0x0f {
		return
	}
	m.sendCommand(OPIntensity, intensity)
}

func (m *MAX7219) SetScanLimit(scanLimit uint8) {
	if scanLimit > 7 {
		return
	}
	m.sendCommand(OPScanLimit, scanLimit)
}

func (m *MAX7219) Shutdown(shutdown bool) {
	v := uint8(1)
	if shutdown {
		v = 0
	}
	m.sendCommand(OPShutdown, v)
}

func (m *MAX7219) DisplayTest(displayTest bool) {
	v := uint8(0)
	if displayTest {
		v = 1
	}
	m.sendCommand(OPDisplayTest, v)
}

func (m *MAX7219) DecodeMode(decodeMode bool) {
	v := uint8(0)
	if decodeMode {
		v = 1
	}
	m.sendCommand(OPDecodeMode, v)
}

func (m *MAX7219) SetRow(row int, v uint8) {
	if row > 7 {
		return
	}
	m.sendCommand(rows[row], v)
}

func (m *MAX7219) ClearDisplay() {
	addr := m.addr
	for i := 0; i < 8; i++ {
		// selecting is needed because otherwise we'd target the first chip
		// after resetting the first row of the second chip
		m.Select(addr).SetRow(i, 0)
	}
}
