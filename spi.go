package ledgontrol

import "github.com/stianeikeland/go-rpio/v4"

type BitOrder int

const (
	MSBFirst = iota
	LSBFirst = iota
)

// see https://github.com/arduino/ArduinoCore-avr/blob/2f67c916f6ab6193c404eebe22efe901e0f9542d/cores/arduino/wiring_shift.c
// ShiftOut will shift data into the register by writing to dPin and pulling the clkPin high then low
// This is a software implementation and thus is slower than actual SPI, although for usage with led matrices this makes no significant difference.
// It can work with any pin, thus it doesn't require you to stick to the SPI pins of the rpi, leaving them available to use for other projects.
func ShiftOut(dPin, clkPin rpio.Pin, bitOrder BitOrder, v uint8) {
	for i := 0; i < 8; i++ {
		pinState := rpio.Low
		if bitOrder == LSBFirst {
			if v&1 != 0 {
				pinState = rpio.High
			}
			v >>= 1
		} else {
			if v&128 != 0 {
				pinState = rpio.High
			}
			v <<= 1
		}
		dPin.Write(pinState)
		clkPin.High()
		clkPin.Low()
	}
}
